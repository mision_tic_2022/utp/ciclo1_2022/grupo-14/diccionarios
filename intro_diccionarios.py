
#Diccionario vacío
miDiccionario = {}

print(miDiccionario)

print("---------------------------------")

#Diccionario con datos
miDiccionario = {
    'clave_1': 'Valor, el valor es un texto',#esto es un item 
    'clave_2': 10,
    'clave_3': 3.1516,
    'clave_4': True
}

print(miDiccionario)

print("---------------------------------")

#Obtener el valor de una clave/key del diccionario
val_1 = miDiccionario['clave_1']
print(val_1)
val_2 = miDiccionario['clave_2']
print(val_2)
val_3 = miDiccionario['clave_4']
print(val_3)

print("---------------------------------")

estudiante_1 = {
    'nombre': 'Jair',
    'apellido': 'Torres',
    'edad': 25,
    'cedula': '12345',
    'promedio_notas': 4.5,
    'activo': True
}
estudiante_2 = {
    'nombre': 'Cecilia',
    'apellido': 'Calderón',
    'edad': 28,
    'cedula': '54321',
    'promedio_notas': 0,
    'activo': False
}

estudiantes = {
    '12345': estudiante_1,
    '54321': estudiante_2
}

print(estudiantes)

print("---------------------------------")

#Accediendo a los valores
dict_estudiante = estudiantes['12345']
print(dict_estudiante)
promedio_notas = dict_estudiante['promedio_notas'] 
nombre_estudiante = dict_estudiante['nombre']
print(f'El promedio de notas del estudiante {nombre_estudiante} es de {promedio_notas}')

print("------------AÑADIR ITEMS AL DICCIONARIO-----------------")
print('Sin el nuevo item')
print(estudiantes)
estudiantes['6789'] = {
    'nombre': 'Jonathan',
    'apellido': 'Carvajal',
    'edad': 26,
    'cedula': '6789',
    'promedio_notas': 0,
    'activo': False
}
print('Con el nuevo item')
print(estudiantes)