'''
1) Calcule el promedio de ventas de cada sede del supermercado representado en el diccionario
2) Calcule las ventas totales del supermercado
'''
supermercado = {
    'belen': {
        'aseo': 12000000,
        'frutas': 24000000,
        'trabajadores':{
            'trabajador_1': {
                'nombre': 'Alberto',
                'apellido': 'Torres',
                'edad': 24
            },
            'trabajador_2': {
                'nombre': 'Ana',
                'apellido': 'Perez',
                'edad': 32
            }
        }
    },
    'estadio':{
        'aseo': 10000000,
        'frutas': 8000000,
        'trabajadores':{
            'trabajador_1': {
                'nombre': 'Fabián',
                'apellido': 'Carvajal',
                'edad': 35
            },
            'trabajador_2': {
                'nombre': 'María',
                'apellido': 'Ortíz',
                'edad': 38
            }
        }
    }
}

#Solución punto 1 y 2 
#SOlución de Juan Pablo
belen=supermercado['belen']
estadio=supermercado['estadio']

#Pormedio ventas por supermercado
promedio_belen=(belen['frutas']+belen['aseo'])/2
print(f"Las ventas en el supermercado de belen es: {promedio_belen}")

promedio_estadio=(estadio['aseo']+estadio['frutas'])/2
print(f"Las ventas en el supermercado de estadio es: {promedio_estadio}")

#Promedio ventas
ventas_supermercado=belen['frutas']+belen['aseo']+estadio['frutas']+estadio['aseo']
print(f"El promedio de ventas del supermercado es de: {ventas_supermercado}")

print('-------------------------PUNTO 3---------------------------------')
'''
3) Desarrolle una función que reciba como parámetro el diccionario anterior y retorne
    el promedio de edad de todos los empleados del supermercado
'''
#Solución de Emmanuel Escobar
def empleados(supermercado):
    return (supermercado['belen']['trabajadores']['trabajador_1']['edad'] + supermercado['belen']['trabajadores']['trabajador_2']['edad'] + supermercado['estadio']['trabajadores']['trabajador_1']['edad'] + supermercado['estadio']['trabajadores']['trabajador_2']['edad'])/4

print(f"El promedio de edad de los empleados es: {empleados(supermercado)}")


