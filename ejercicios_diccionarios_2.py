
estudiantes = {
    '12345': {
        'nombre': 'Jair',
        'apellido': 'Torres',
        'edad': 25,
        'notas': {
            'nota_1': 3.4,
            'nota_2': 4.5,
            'nota_3': 4.9,
            'nota_4': 3.1
        }
    },
    '54321': {
        'nombre': 'Cecilia',
        'apellido': 'Calderón',
        'edad': 28,
        'notas': {
            'nota_1': 3.1,
            'nota_2': 4.1,
            'nota_3': 4.3,
            'nota_4': 3.9
        }
    },
    '22441': {
        'nombre': 'Ricardo',
        'apellido': 'Castro',
        'edad': 32,
        'notas': {
            'nota_1': 3.5,
            'nota_2': 4.8,
            'nota_3': 2.2,
            'nota_4': 1.4
        }
    }
}

'''
*Calcular el promedio de notas por estudiante
*Calcular el promedio de notas general, es decir, de todos los estudiantes
'''

'''
diccionario.values() -> retorna los valores del diccionario
diccionario.items() -> retorna las claves y valores del diccionario
'''

'''
#Punto 1
def calcular_promedio_notas(dict_estudiantes: dict):
    # Diccionario que almacena el promedio de notas de los estudiantes
    dict_promedio_notas = dict()
    #Variable que representa el promedio general
    promedio_general = 0
    # est-> representa a cada estudiante del diccionario
    # Iterar el diccionario padre (estudiantes)
    for cedula, est in dict_estudiantes.items():
        #print('Cédula: ', cedula)
        # Obtener las notas del estudiante
        notas = est['notas']
        # print(notas)
        promedio_notas = 0
        # Recorrer el diccionario notas
        for n in notas.values():
            promedio_notas += n
            # print(n)
        # len(diccionario) -> Obtener el tamaño del diccionario 'notas'
        cant_notas = len(notas)
        #print('Cantidad de notas: ', cant_notas)
        # Calcular el promedio de notas del estudiante
        promedio_notas /= cant_notas
        # print(promedio_notas)
        # Añadir promedio notas al diccionario
        dict_promedio_notas[cedula] = promedio_notas
    
    return dict_promedio_notas
'''

#Punto 2 (modificación/anexo del punto 1)
#Solución de Luis Alejandro
def calcular_promedio_notas(dict_estudiantes: dict):
    # Diccionario que almacena el promedio de notas de los estudiantes
    dict_promedio_notas = dict()
    # est-> representa a cada estudiante del diccionario
    # Iterar el diccionario padre (estudiantes)
    for cedula, est in dict_estudiantes.items():
        #print('Cédula: ', cedula)
        # Obtener las notas del estudiante
        notas = est['notas']
        # print(notas)
        promedio_notas = 0
        # Recorrer el diccionario notas
        for n in notas.values():
            promedio_notas += n
            # print(n)
        # len(diccionario) -> Obtener el tamaño del diccionario 'notas'
        cant_notas = len(notas)
        #print('Cantidad de notas: ', cant_notas)
        # Calcular el promedio de notas del estudiante
        promedio_notas /= cant_notas
        # print(promedio_notas)
        # Añadir promedio notas al diccionario
        dict_promedio_notas[cedula] = promedio_notas
    promedio_notas_general = 0
    for k in dict_promedio_notas.values():
        # print(k)
        #Suma el promedio de cada estudiante
        promedio_notas_general += k
    #Obtiene el tamaño del diccionario dict_promedio_notas y calcula el promedio general
    promedio_notas_general /= len(dict_promedio_notas)
    #Añade el promedio general al diccionario a retornar
    dict_promedio_notas["Promedio_notas_general"] = promedio_notas_general
    # print(promedio_notas_general)
    return dict_promedio_notas


print(calcular_promedio_notas(estudiantes))

