
#Crear un diccionario vacío
#diccionario = dict()
otroDiccionario = {}

diccionario = {
    'nombre': 'Carlos'
}

#Añadir elementos al diccionario
diccionario['nombre'] = 'Andrés'

print(diccionario)

'''
Si la clave no existe en el diccionario entonces la creará, de lo contrario
sobreescribe los valores
'''