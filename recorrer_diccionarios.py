
supermercado_1 = {
    'belen': {
        'aseo': 12000000,
        'frutas': 24000000
    },
    'estadio':{
        'aseo': 10000000,
        'frutas': 8000000
    },
    'chapinero':{
        'aseo': 20000000,
        'frutas': 18000000
    }
}

#Recorrer diccionario obteniendo la clave
for x in supermercado_1:
    print(f'clave: {x}')
    valor = supermercado_1[x]
    print(valor)

print('---------------Obteniendo el valor---------------------')

#Recorrer diccionario obteniendo el valor
for valor in supermercado_1.values():
    print(valor)

print('----------------Obteniendo clave - valor--------------------')

#Recorrer diccionario obteniendo clave - valor
for clave, valor in supermercado_1.items():
    print(f'Clave: {clave} - valor: {valor}')


supermercado_2 = {
    'belen': {
        'aseo': 12000000,
        'frutas': 24000000,
        'trabajadores':{
            'trabajador_1': {
                'nombre': 'Alberto',
                'apellido': 'Torres',
                'edad': 24
            },
            'trabajador_2': {
                'nombre': 'Ana',
                'apellido': 'Perez',
                'edad': 32
            }
        }
    },
    'estadio':{
        'aseo': 10000000,
        'frutas': 8000000,
        'trabajadores':{
            'trabajador_1': {
                'nombre': 'Fabián',
                'apellido': 'Carvajal',
                'edad': 35
            },
            'trabajador_2': {
                'nombre': 'María',
                'apellido': 'Ortíz',
                'edad': 38
            }
        }
    }
}

'''
Mostrar las claves y los valores del diccionario y los subdiccionarios
'''

